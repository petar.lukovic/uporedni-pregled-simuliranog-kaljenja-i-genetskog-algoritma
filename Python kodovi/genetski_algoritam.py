import sys
SCRIPTS_PATH = 'SAO_projekat/hromozom.py'
sys.path.insert(0, SCRIPTS_PATH)
from hromozom import *
import random
import math as m
import time 
import numpy as np
import matplotlib.pyplot as plt

class Populacija:
    
    def __init__(self):
        self.broj_bita_ = 13
        self.broj_iteracija_ = 500
        self.broj_jedinki_ = 300
        self.broj_odbiraka_x_ = int(pow(2, self.broj_bita_))
        self.broj_odbiraka_y_ = int(pow(2, self.broj_bita_))
        self.x_min_ = -1.0
        self.x_max_ = 5.0
        self.y_min_ = -1.0
        self.y_max_ = 5.0
        #self.x_min_ = -2.0
        #self.x_max_ = 0.0
        #self.y_min_ = -1.0
        #self.y_max_ = 1.0
        self.minimum_ = Hromozom()
        self.maksimum_ = Hromozom()
        self.populacija_ = [None] * self.broj_jedinki_
        self.trenutni_maksimum_ = -100000.
        self.trenutni_minimum_ = 100000.
        self.ukupna_udaljenost_ = 0.
        self.srednja_udaljenost_ = [None] * self.broj_iteracija_
        self.delta_x_ = (self.x_max_ - self.x_min_) / self.broj_odbiraka_x_
        self.delta_y_ = (self.y_max_ - self.y_min_) / self.broj_odbiraka_y_        
        self.x_osa_ = np.linspace(self.x_min_, self.x_max_, self.broj_odbiraka_x_)
        self.y_osa_ = np.linspace(self.y_min_, self.y_max_, self.broj_odbiraka_y_)
        self.x_matrica_, self.y_matrica_ = np.meshgrid(self.x_osa_, self.y_osa_)
        self.funkcija_ = self.funkcija(self.x_matrica_, self.y_matrica_)

        temp = Hromozom()
        for i in range(self.broj_jedinki_):
            self.populacija_[i] = temp.generisiRandomHromozom()

    #def funkcija(self, x, y):
    #    return x * np.exp(- np.power(x, 2) - np.power(y, 2))
    
    def funkcija(self, x, y):
        return (3./2.) * np.exp(1./(1. + np.power(x - 1, 2) + np.power(y - 1, 2))) - (5./2.) * np.exp(1./(1. + (1./4.) * np.power(x + 1./2., 2) + (1./36.)*np.power(y - 1, 2))) + (2.) * np.exp(1./(1. + np.power(x - 2, 2) + np.power(y - 2, 2))) + (2.) * np.exp(1./(1. + np.power(x - 1, 2) + np.power(y + 1, 2)))
    
    def nadjiMinimum(self):
        
        for j in range(self.broj_iteracija_):
            self.populacija_ = self.favorizuj()
            self.rekombinuj()
            self.mutiraj()
            self.srednja_udaljenost_[j] = self.ukupna_udaljenost_ / self.broj_jedinki_
            #if j == self.broj_iteracija_ - 1:
            #    self.crtaj(j)
                
        #plt.plot(self.srednja_udaljenost_)
        #plt.xticks(np.arange(0, self.broj_iteracija_ + 10, step = 20))
        #plt.show()
        
        #for i in range(self.broj_jedinki_):
        #    if self.funkcija_[self.minimum_.uIndeks(self.minimum_.getX())][self.minimum_.uIndeks(self.minimum_.getY())] > self.funkcija_[self.populacija_[i].uIndeks(self.populacija_[i].getX())][self.populacija_[i].uIndeks(self.populacija_[i].getY())]:
        #        self.minimum_ = self.populacija_[i]

        #print("Minimum je: " + str(self.funkcija_[self.minimum_.uIndeks(self.minimum_.getX())][self.minimum_.uIndeks(self.minimum_.getY())]) + " u tacki : (" + str(self.x_min_ + self.minimum_.uIndeks(self.minimum_.getY())*self.delta_x_) + ", " + str(self.y_min_ + self.minimum_.uIndeks(self.minimum_.getX())*self.delta_y_) + ")")
        #print(self.minimum_.toString())
        
        for i in range(self.broj_jedinki_):
            if self.funkcija_[self.maksimum_.uIndeks(self.maksimum_.getX())][self.maksimum_.uIndeks(self.maksimum_.getY())] < self.funkcija_[self.populacija_[i].uIndeks(self.populacija_[i].getX())][self.populacija_[i].uIndeks(self.populacija_[i].getY())]:
                self.maksimum_ = self.populacija_[i]

        print("Maksimum je: " + str(self.funkcija_[self.maksimum_.uIndeks(self.maksimum_.getX())][self.maksimum_.uIndeks(self.maksimum_.getY())]) + " u tacki : (" + str(self.x_min_ + self.maksimum_.uIndeks(self.maksimum_.getY())*self.delta_x_) + ", " + str(self.y_min_ + self.maksimum_.uIndeks(self.maksimum_.getX())*self.delta_y_) + ")")
        print(self.maksimum_.toString())
        
    def favorizuj(self):
        self.ugradiUdaljenost()
        suma = 0.
        novaPopulacija = [None] * self.broj_jedinki_
        
        for i in range(self.broj_jedinki_):
            rand = random.random() * self.ukupna_udaljenost_;
            for k in range(self.broj_jedinki_):
                temp = suma + self.populacija_[k].getUdaljenost(); 
                if ((suma <= rand) and (temp >= rand)):
                    novaPopulacija[i] = Hromozom()
                    novaPopulacija[i].setX(self.populacija_[k].getX())
                    novaPopulacija[i].setY(self.populacija_[k].getY())
                    break
                suma += self.populacija_[k].getUdaljenost()
            suma = 0.
                
        return novaPopulacija;
    
    def crtaj(self, k):
        ax = plt.axes(projection = '3d')    
        ax.plot_surface(self.x_matrica_, self.y_matrica_, self.funkcija_)
        for i in range(self.broj_jedinki_):
            x_koordinata = self.y_osa_[self.populacija_[i].uIndeks(self.populacija_[i].getX())]
            y_koordinata = self.x_osa_[self.populacija_[i].uIndeks(self.populacija_[i].getY())]
            vrednost = self.funkcija_[self.populacija_[i].uIndeks(self.populacija_[i].getX())][self.populacija_[i].uIndeks(self.populacija_[i].getY())] + 0.1
            ax.scatter(y_koordinata, x_koordinata, vrednost, c = 'r')
        ax.set_ylabel("y - osa")
        ax.set_xlabel("x - osa")
        ax.set_xticks(np.arange(-20, 20, step = 0.5))
        ax.set_yticks(np.arange(-20, 20, step = 0.5))
        ax.set_zticks(np.arange(-20, 20, step = 0.5))
        ax.set_title("genetski algoritam generacija: " + str(k + 1))
        plt.show()
                
    def ugradiUdaljenost(self):
        self.ukupna_udaljenost_ = 0.
        self.trenutni_minimum_ = 10000.
        self.trenutni_maksimum_ = -10000.
        
        #for i in range(self.broj_jedinki_):
        #    if self.funkcija_[self.populacija_[i].uIndeks(self.populacija_[i].getX())][self.populacija_[i].uIndeks(self.populacija_[i].getY())] >= self.trenutni_maksimum_ :
        #        self.trenutni_maksimum_ = self.funkcija_[self.populacija_[i].uIndeks(self.populacija_[i].getX())][self.populacija_[i].uIndeks(self.populacija_[i].getY())]
    
        #for i in range(self.broj_jedinki_):
        #    self.populacija_[i].setUdaljenost(abs(self.trenutni_maksimum_ - self.funkcija_[self.populacija_[i].uIndeks(self.populacija_[i].getX())][self.populacija_[i].uIndeks(self.populacija_[i].getY())]))
        #    self.ukupna_udaljenost_ += self.populacija_[i].getUdaljenost()
            
        for i in range(self.broj_jedinki_):
            if self.funkcija_[self.populacija_[i].uIndeks(self.populacija_[i].getX())][self.populacija_[i].uIndeks(self.populacija_[i].getY())] <= self.trenutni_minimum_ :
                self.trenutni_minimum_ = self.funkcija_[self.populacija_[i].uIndeks(self.populacija_[i].getX())][self.populacija_[i].uIndeks(self.populacija_[i].getY())]

        for i in range(self.broj_jedinki_):
            self.populacija_[i].setUdaljenost(abs(self.trenutni_minimum_ - self.funkcija_[self.populacija_[i].uIndeks(self.populacija_[i].getX())][self.populacija_[i].uIndeks(self.populacija_[i].getY())]))
            self.ukupna_udaljenost_ += self.populacija_[i].getUdaljenost()
            
    def mutiraj(self):
        for i in range(self.broj_jedinki_):
            self.populacija_[i].mutacija()
    
    def rekombinuj(self):
        for i in range(self.broj_jedinki_):
            indeks1 = random.randrange(0, self.broj_jedinki_)
            indeks2 = random.randrange(0, self.broj_jedinki_)
            self.populacija_[indeks1].rekombinacija(self.populacija_[indeks2])



start = time.time()

for i in range(1):
    print("----------     RUNDA " + str(i + 1) + "     ----------")
    proba = Populacija()
    proba.nadjiMinimum() 
    
end = time.time()
print("Vreme izvrsavanja: " + str(end - start))                