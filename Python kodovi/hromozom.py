import random
import math as m

class Hromozom:

    def __init__(self):
        self.broj_bita_ = 13
        self.verovatnoca_mutacija_ = 0.01
        #self.verovatnoca_mutacija_ = 1.
        self.udaljenost_ = 0.
        self.y_ = self.generisiRandomString()
        self.x_ = self.generisiRandomString()
        
    def getX(self):
        return self.x_
    
    def getY(self):
        return self.y_
    
    def setX(self, x):
        self.x_ = x 
        
    def setY(self, y):
        self.y_ = y 
    
    def setUdaljenost(self, udaljenost):
        self.udaljenost_ = udaljenost
    
    def getUdaljenost(self):
        return self.udaljenost_
    
    def generisiRandomString(self):
        binarna_rec = ''
        jedan = '1'
        nula = '0'
        for i in range(self.broj_bita_):
            if random.random() < 0.5 :
                binarna_rec = binarna_rec + nula
            else: 
                binarna_rec = binarna_rec + jedan

        return binarna_rec

    def generisiRandomHromozom(self):
        return Hromozom()
    
    def mutacija(self):
        if random.random() < self.verovatnoca_mutacija_ : 
            self.izmeni()

    def izmeni(self):
        rand = random.randrange(0, self.broj_bita_)
        #print("Mutira se bit broj " + str(rand) + " ---->")
        
        x1 = ''
        for i in range(self.broj_bita_):
            if i != rand:
                x1 += self.x_[i]
            else: 
                x1 += '1' if self.x_[rand] == '0' else '0'
        self.x_ = x1
        
        x1 = ''
        for i in range(self.broj_bita_):
            if i != rand:
                x1 += self.y_[i]
            else: 
                x1 += '1' if self.y_[rand] == '0' else '0'
        self.y_ = x1

        
    def uIndeks(self, binarna_rec):
        indeks = 0
        for i in range(self.broj_bita_):
            bit = 0
            if(binarna_rec[i] == "1"):
                bit = 1
            else:
                bit = 0
            indeks += bit * m.pow(2, i)

        return int(indeks)

    def rekombinacija(self, drugi):
        indeks1 = random.randrange(0, self.broj_bita_) 
        indeks2 = random.randrange(0, self.broj_bita_)
        
        #print("Mesto ukrstanja hromozoma x je " + str(indeks1) + ", ")
        #print("Mesto ukrstanja hromozoma y je " + str(indeks2) + "---->")
        
        x1 = self.x_[0: indeks1 + 1]
        x2 = self.x_[indeks1 + 1: self.broj_bita_]
        x3 = drugi.getX()[0: indeks1 + 1]
        x4 = drugi.getX()[indeks1 + 1: self.broj_bita_]

        y1 = self.y_[0: indeks2 + 1]
        y2 = self.y_[indeks2 + 1: self.broj_bita_]
        y3 = drugi.getY()[0: indeks2 + 1]
        y4 = drugi.getY()[indeks2 + 1: self.broj_bita_]

        self.x_ = x3 + x2
        self.y_ = y3 + y2
        drugi.setX(x1 + x4)
        drugi.setY(y1 + y4)
        
    #def toString(self):
    #    return 'Hromozom [x = ' + str(self.uIndeks(self.y_)) + ', y = ' + str(self.uIndeks(self.x_)) + ']'
    
    def toString(self):
        return 'Hromozom [x = ' + self.x_ + ', y = ' + self.y_ + '][x = ' + str(self.uIndeks(self.y_)) + ', y = ' + str(self.uIndeks(self.x_)) + ']'
    
#print("Slucajno generisanje dva hromozoma...")    
#hromozom1 = Hromozom()
#hromozom2 = Hromozom()
#print(hromozom1.toString())
#print(hromozom2.toString())
#print("\n")

#print("Mutacija dva hromozoma sa verovatnocom 1...") 
#hromozom1.mutacija()
#print(hromozom1.toString())
#hromozom2.mutacija()
#print(hromozom2.toString())
#print("\n")

#print("Rekombinacija dva hromozoma...")
#hromozom1.rekombinacija(hromozom2)
#print(hromozom1.toString())
#print(hromozom2.toString())
#print("\n")