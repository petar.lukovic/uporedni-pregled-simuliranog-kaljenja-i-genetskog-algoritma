package GA;

import java.util.Random;

public class Hromozom {
	static int brojBita;
	String x;
	String y;
	double udaljenost;
	final double verovatnocaMutacije = 0.001;
	
	public Hromozom() {
		brojBita = 13;
		x = new String(giveRandomString());
		y = new String(giveRandomString());
		this.udaljenost = 0.;
	}
	public Hromozom(String x, String y) {
		brojBita = 13;
		this.x = x;
		this.y = y;
		this.udaljenost = 0.;
	}
	public String getX() {
		return x;
	}
	public void setX(String x) {
		this.x = x;
	}
	public String getY() {
		return y;
	}
	public void setY(String y) {
		this.y = y;
	}
	public double getUdaljenost() {
		return udaljenost;
	}
	public void setUdaljenost(double udaljenost) {
		this.udaljenost = udaljenost;
	}
	
	static Hromozom generisiHromozom() {
		String newX = new String(giveRandomString());
		String newY = new String(giveRandomString());
		return new Hromozom(newX, newY);
	}
	
	public static String giveRandomString() {
		String binarnaRec = new String();
		for(int i = 0; i < brojBita; i++) {
			if(Math.random() < 0.5) binarnaRec += "0";
			else binarnaRec += "1";
		}
		return binarnaRec;
	}
	
	public void mutacija() {
		if(Math.random() < verovatnocaMutacije) {
			this.x = izmeni(this.x);
		}
		if(Math.random() < verovatnocaMutacije) {
			this.y = izmeni(this.y);
		}
	}
	
	private String izmeni(String rec) {
		Random rand = new Random();
		int indeks = rand.nextInt(brojBita);
		String novi = new String();
		for(int i = 0; i < brojBita; i++) {
			if(i == indeks) {
				if(rec.charAt(indeks) == '0') novi += '1';
				else novi += '0';
			}
			else novi += rec.charAt(i);
		}
		return novi;
	}
	
	public int uIndeks(String binarnaRec) {
		int indeks = 0;
		for(int i = 0; i < brojBita; i++) {
			int bit = (binarnaRec.charAt(i) == '0') ? 0 : 1;
			indeks += bit * Math.pow(2, i);
		}
		return indeks;
	}
	
	public void rekombinacija(Hromozom drugi) {
		Random rand = new Random();
		int indeks1 = rand.nextInt(brojBita);	
		int indeks2 = rand.nextInt(brojBita);
		
		String x1 = new String(this.x.substring(0, indeks1 + 1));
		String x2 = new String(this.x.substring(indeks1 + 1, brojBita));
		String x3 = new String(drugi.x.substring(0, indeks1 + 1));
		String x4 = new String(drugi.x.substring(indeks1 + 1, brojBita));

		String y1 = new String(this.y.substring(0, indeks2 + 1));
		String y2 = new String(this.y.substring(indeks2 + 1, brojBita));
		String y3 = new String(drugi.y.substring(0, indeks2 + 1));
		String y4 = new String(drugi.y.substring(indeks2 + 1, brojBita));
		
		this.x = x1.concat(x4);
		this.y = y1.concat(y4);
		
		drugi.setX(x3.concat(x2));
		drugi.setY(y3.concat(y2));
		
		//if(drugi.getUdaljenost() > this.udaljenost) {
		//	this.x = x1.concat(x4);
		//	this.y = y1.concat(y4);
		//} else {
		//	drugi.setX(x3.concat(x2));
		//	drugi.setY(y3.concat(y2));
		//}
	}
	
	@Override
	public String toString() {
		return "Hromozom [x = " + uIndeks(x) + ", y = " + uIndeks(y) + "]";
	}
	
	public static void main(String[] args) {
		Hromozom hromozom1 = new Hromozom("0010110001111", "0000011100101");
		Hromozom hromozom2 = new Hromozom("1001011010010", "1010101010100");
		System.out.println(hromozom1);
		System.out.println(hromozom2);
		
		hromozom1.mutacija();
		hromozom2.mutacija();
		System.out.println(hromozom1);
		System.out.println(hromozom2);
		
		hromozom1.rekombinacija(hromozom2);
		System.out.println(hromozom1);
		System.out.println(hromozom2);
	}
}
