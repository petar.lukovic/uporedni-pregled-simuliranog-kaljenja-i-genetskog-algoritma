package GA;

import java.util.Random;

public class Populacija {
	
	final int brojIteracija = 500;
	final int brojJedinki = 500;
	
	final int brojOdbirakaX = 8192;// 2^13
	final int brojOdbirakaY = 8192; // 2^13
	final double xMin = -2;
	final double xMax = 0.;
	final double yMin = -1;
	final double yMax = 1;
	double deltaX;
	double deltaY;

	double funkcija[][];
	Hromozom populacija[];
	Hromozom minimum;
	double trenutniMaksimum;
	double ukupnaUdaljenost;
	
	public Populacija() {
		minimum = new Hromozom();
		funkcija = new double[brojOdbirakaX][brojOdbirakaY];
		populacija = new Hromozom[brojJedinki];
		trenutniMaksimum = Double.NEGATIVE_INFINITY;
		ukupnaUdaljenost = 0.;
		
		deltaX = 2./brojOdbirakaX;
		deltaY = 2./brojOdbirakaY;
		for(int i = 0; i < brojOdbirakaX; i++) {
			for(int j = 0; j < brojOdbirakaY; j++) {
				funkcija[i][j] = (xMin + i*deltaX)
						*Math.pow(Math.E, -(Math.pow((xMin + i*deltaX), 2) + Math.pow((yMin + j*deltaY), 2)));
			}
		}
		
		for(int i = 0; i < brojJedinki; i++) {
			populacija[i] = Hromozom.generisiHromozom();
		}
	}
	
	public Hromozom nadjiMinimum() {
		for(int i = 0; i < brojIteracija; i++) {
			populacija = favorizuj();
			rekombinuj();
			mutiraj();
		}
		
		for(int i = 0; i < brojJedinki; i++) {
			if(funkcija[minimum.uIndeks(minimum.getX())][minimum.uIndeks(minimum.getY())] >
			funkcija[populacija[i].uIndeks(populacija[i].getX())][populacija[i].uIndeks(populacija[i].getY())]) {
				minimum = populacija[i];
			}
		}
		
		System.out.println("Minimum je: " + funkcija[minimum.uIndeks(minimum.getX())][minimum.uIndeks(minimum.getY())] + 
				" u tacki : (" + (xMin + minimum.uIndeks(minimum.getX())*deltaX) 
				+ ", " + (yMin + minimum.uIndeks(minimum.getY())*deltaY) + ")");
		return minimum;		
	}
	
	public Hromozom[] favorizuj() {
		ugradiUdaljenosti();
		Hromozom novaPopulacija[] = new Hromozom[brojJedinki];
		double suma = 0.;
		for(int j = 0; j < brojJedinki; j++) {
			double random = Math.random() * ukupnaUdaljenost;
			for(int k = 0; k < brojJedinki; k++) {
				double temp = suma + populacija[k].getUdaljenost(); 
				if((suma <= random) && (temp >= random)) {
					novaPopulacija[j] = 
							new Hromozom(new String(populacija[k].getX()), new String(populacija[k].getY()));
					break;
				}
				suma += populacija[k].getUdaljenost();
			}
			suma = 0.;
		}
		return novaPopulacija;
	}
	
	public void ugradiUdaljenosti() {
		ukupnaUdaljenost = 0.;
		trenutniMaksimum = Double.NEGATIVE_INFINITY;
		
		for(int i = 0; i < brojJedinki; i++) {
			if(funkcija[populacija[i].uIndeks(populacija[i].getX())]
					[populacija[i].uIndeks(populacija[i].getY())] >= trenutniMaksimum) {
				trenutniMaksimum = funkcija[populacija[i].uIndeks(populacija[i].getX())]
						[populacija[i].uIndeks(populacija[i].getY())];
			}
		}	
		
		for(int i = 0; i < brojJedinki; i++) {
			populacija[i].setUdaljenost(Math.abs(trenutniMaksimum - 
					funkcija[populacija[i].uIndeks(populacija[i].getX())][populacija[i].uIndeks(populacija[i].getY())]));
			ukupnaUdaljenost += populacija[i].getUdaljenost();
			
			/*favorizacija sa e na rastojanje od maksimuma
			  populacija[i].setUdaljenost(Math.pow(Math.E, 
					Math.abs(trenutniMaksimum - funkcija[populacija[i].uIndeks(populacija[i].getX())]
						[populacija[i].uIndeks(populacija[i].getY())])));
			ukupnaUdaljenost += populacija[i].getUdaljenost();*/
		}
	}

	private void mutiraj() {
		for(int i = 0; i < brojJedinki; i++) {
			populacija[i].mutacija();
		}	
	}

	private void rekombinuj() {
		for(int j = 0; j < brojJedinki / 2; j++) {
			Random rand = new Random();
			int indeks1 = rand.nextInt(brojJedinki);
			int indeks2 = rand.nextInt(brojJedinki);
			populacija[indeks1].rekombinacija(populacija[indeks2]);
		}		
	}

	public static void main(String[] args) {
		long pocetak = System.currentTimeMillis();
	
		for(int i = 0; i < 1; i++) {
			System.out.println("----------     RUNDA " + (i+1) + "     ----------");
			Populacija proba = new Populacija();
			pocetak = System.currentTimeMillis();
			System.out.println(proba.nadjiMinimum());
		}
		
		long kraj = System.currentTimeMillis();
		long protekloVreme = kraj - pocetak;
		System.out.println("Vreme izvrsavanja: " + protekloVreme + "ms...");
	}

}
