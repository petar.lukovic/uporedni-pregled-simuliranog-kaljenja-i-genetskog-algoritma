package simuliranoKaljenje;

public class Resenje {
	int indeksX;
	int indeksY;
	double vrednost;
	
	public Resenje(int indeksX, int indeksY, double vrednost) {
		this.indeksX = indeksX;
		this.indeksY = indeksY;
		this.vrednost = vrednost;
	}
	public int getIndeksX() {
		return indeksX;
	}
	public void setIndeksX(int indeksX) {
		this.indeksX = indeksX;
	}
	public int getIndeksY() {
		return indeksY;
	}
	public void setIndeksY(int indeksY) {
		this.indeksY = indeksY;
	}
	public double getVrednost() {
		return vrednost;
	}
	public void setVrednost(double vrednost) {
		this.vrednost = vrednost;
	}
	
	@Override
	public String toString() {
		return "Minimum funkcije je: x = " + (-2. + indeksX * 0.0002) 
				+ " y = " + (-1. + indeksY * 0.0002) + " vrednost = " + vrednost;
	}
}
