package simuliranoKaljenje;
import java.util.Random;

public class SimuliranoKaljenje {

	//parametri algoritma
	//dobra resenja :
	//0.98 - 500
	//0.99996 - bez ponavljanja
	final double koeficijentHladjenja = 0.99998;
	final int brojIteracija = 200;
	final double minimalnaTemperatura = .0001;
	final double pocetnaTemperatura = 1.;
	
	//parametri na kom opsegu i sa koliko odbiraka se trazi esktremum
	final int brojOdbirakaX = 10000;
	final int brojOdbirakaY = 10000;
	final double xMin = -2.;
	final double xMax = 0.;
	final double yMin = -1.;
	final double yMax = 1.;
	
	//diskretan signal i ekstremum
	double funkcija[][];
	Resenje minimum;
	
	public void diskretizacija() {
		double deltaX = 2./brojOdbirakaX;
		double deltaY = 2./brojOdbirakaY;
		funkcija = new double[brojOdbirakaX][brojOdbirakaY];
		for(int i = 0; i < brojOdbirakaX; i++) {
			for(int j = 0; j < brojOdbirakaY; j++) {
				funkcija[i][j] = (xMin + i*deltaX)
						*Math.pow(Math.E, -(Math.pow((xMin + i*deltaX), 2) + Math.pow((yMin + j*deltaY), 2)));
			}
		}
	}

	public Resenje nadjiMinimum() {
		int brojIzvrsavanja = 0;
		double trenutnaTemperatura = pocetnaTemperatura;
		Resenje trenutnoResnje = slucajniPocetak();
		
		while(trenutnaTemperatura > minimalnaTemperatura) {
			//for(int i = 0; i < brojIteracija; i++) {
				Resenje komsija = generisiKomsiju(trenutnoResnje);
				double kriticnaVrednost = Math.pow(Math.E, 
						(trenutnoResnje.getVrednost() - komsija.getVrednost()) / trenutnaTemperatura);
				if(komsija.getVrednost() < trenutnoResnje.getVrednost()) trenutnoResnje = komsija;
				else if(kriticnaVrednost > Math.random()) trenutnoResnje = komsija;
				brojIzvrsavanja++;
			//}
			trenutnaTemperatura *= koeficijentHladjenja;
		}
		
		minimum = trenutnoResnje;
		System.out.println("Broj izvrsavanja je: " + brojIzvrsavanja);
		return trenutnoResnje;
	}
	
	public Resenje generisiKomsiju(Resenje stari) {
		Random random = new Random();
		int trenutni1 = random.nextInt(3) - 1;
		int trenutni2 = random.nextInt(3) - 1;
		
		if(trenutni1 == -1 && stari.getIndeksX() == 0 ||
				trenutni1 == 1 && stari.getIndeksX() == (brojOdbirakaX - 1)) trenutni1 = 0; 
		if(trenutni2 == -1 && stari.getIndeksY() == 0 ||
				trenutni2 == 1 && stari.getIndeksY() == (brojOdbirakaY - 1)) trenutni2 = 0;
		
		int randomIndeksX = stari.getIndeksX() + trenutni1;
		int randomIndeksY = stari.getIndeksY() + trenutni2;
		return new Resenje(randomIndeksX, randomIndeksY, funkcija[randomIndeksX][randomIndeksY]);
	}

	public Resenje slucajniPocetak() {
		int randomIndeksX = (int)(Math.random() * (brojOdbirakaX - 1));
		int randomIndeksY = (int)(Math.random() * (brojOdbirakaY - 1));
		return new Resenje(randomIndeksX, randomIndeksY, funkcija[randomIndeksX][randomIndeksY]);
	}

	public static void main(String[] args) {
		long pocetak = System.currentTimeMillis();
		int pogresni = 0;
		
		for(int i = 0; i < 5; i++) {
			SimuliranoKaljenje proba = new SimuliranoKaljenje();
			proba.diskretizacija();
			System.out.println( (i+1) + ". " + proba.nadjiMinimum() + "\n");
			if(proba.minimum.getVrednost() > -0.42) pogresni++;
		}
		
		System.out.println("Pogresni : " + pogresni);
		long kraj = System.currentTimeMillis();
		long protekloVreme = kraj - pocetak;
		System.out.println("Vreme izvrsavanja: " + protekloVreme + "ms...");
	}
}
